// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "AggressiveTidy",
    products: [
        .library(name: "AggressiveTidy", targets: ["AggressiveTidy"]),
        ],
    targets: [
        .target(name: "AggressiveTidy", path: "Sources"),
        .testTarget(name: "AggressiveTidyTests", dependencies: ["AggressiveTidy"], path: "Tests"),
        ]
)


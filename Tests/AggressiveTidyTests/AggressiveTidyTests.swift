import XCTest
import Foundation
@testable import AggressiveTidy

class AggressiveTidyTests: XCTestCase {
    func test_uppercase_letters_preserved() {
        let input = "UPPERCASE"
        let output = AggressiveTidy.tidy(input)
        
        XCTAssertEqual(input, output)
    }
    
    func test_lowercase_letters_turned_into_uppercase() {
        let input = "lowercase"
        let output = AggressiveTidy.tidy(input)
        
        XCTAssertEqual("LOWERCASE", output)
    }
    
    func test_numbers_do_not_get_removed() {
        let input = "0123456789"
        let output = AggressiveTidy.tidy(input)
        
        XCTAssertEqual(input, output!)
    }
    
    func test_ampersand_replaced_with_AND() {
        let input = "BUTTER&TOAST"
        let output = AggressiveTidy.tidy(input)
        
        XCTAssertEqual("BUTTERANDTOAST", output)
    }
    
    func test_whitespace_removed() {
        let input = "\t\n "
        let output = AggressiveTidy.tidy(input)
        
        XCTAssertEqual("", output)
    }
    
    func test_nonalphanumeric_chars_removed() {
        let allowedChars = CharacterSet.alphanumerics.union(CharacterSet(charactersIn: "&")).inverted.allCharacters()
        
        for c in allowedChars {
            XCTAssertEqual("", AggressiveTidy.tidy(String(c)))
        }
        
    }
}

fileprivate extension CharacterSet {
    func allCharacters() -> [Character] {
        var result: [Character] = []
        for plane: UInt8 in 0...16 where self.hasMember(inPlane: plane) {
            for unicode in UInt32(plane) << 16 ..< UInt32(plane + 1) << 16 {
                if let uniChar = UnicodeScalar(unicode), self.contains(uniChar) {
                    result.append(Character(uniChar))
                }
            }
        }
        return result
    }
}

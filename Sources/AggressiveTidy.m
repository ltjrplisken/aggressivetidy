//
//  AggressiveTidy.m
//  ReVAPI
//
//  Created by Rohit Natarajan on 27/07/2018.
//

#import <Foundation/Foundation.h>
#import "include/AggressiveTidy.h"
#import <string.h>

@implementation AggressiveTidy: NSObject

+ (bool) isLower:(char)c {
    return (c >= 97 && c <= 122);
}

+ (bool) isAmpersand:(char)c {
    return c == 38;
}

+ (bool) isAlphanumeric:(char)c {
    return (c >= 48 && c <= 57) || (c >= 65 && c <= 90);
}

+ (NSString*) tidy:(NSString*)input {
    const char* cstr = [input UTF8String];
    int count = strlen(cstr);
    char tidied[3*count + 1];
    int j = 0;
    
    for (int i = 0; i<count; i++) {
        char c = cstr[i];
        
        if (c == 38) {
            tidied[j] = 65;
            tidied[j+1] = 78;
            tidied[j+2] = 68;
            j += 3;
        }
        else if (c >= 97 && c <= 122) {
            tidied[j] = c - 32;
            j++;
        }
        else if ((c >= 48 && c <= 57) || (c >= 65 && c <= 90)) {
            tidied[j] = c;
            j++;
        }
    }
    tidied[j] = '\0';
    
    return [NSString stringWithCString:tidied encoding:NSUTF8StringEncoding];
}

@end

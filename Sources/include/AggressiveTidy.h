//
//  AggressiveTidy.h
//  ReVAPI
//
//  Created by Rohit Natarajan on 27/07/2018.
//

#ifndef AggressiveTidy_h
#define AggressiveTidy_h

#import <Foundation/Foundation.h>

@interface AggressiveTidy: NSObject

+ (NSString*) tidy:(NSString*)input;

@end
#endif /* AggressiveTidy_h */
